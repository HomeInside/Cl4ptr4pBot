####################################################################################################
## Builder
## from: https://kerkour.com/rust-small-docker-image
####################################################################################################
FROM rust:182 AS builder

RUN update-ca-certificates

# Create appuser
ENV USER=nouser
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

# Set /app as working directory
WORKDIR /nouser

# copy data from current dir into working directory
COPY ./ .

# We no longer need to use the x86_64-unknown-linux-musl target
RUN cargo build --release

####################################################################################################
## Final image
####################################################################################################
FROM gcr.io/distroless/cc

LABEL maintainer="Jorge Brunal Pérez <diniremix@gmail.com>"

# Import from builder.
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /nouser

# Copy our build
COPY --from=builder /nouser/target/release/Cl4ptr4pBot ./

# Use an unprivileged user.
USER nouser:nouser

ENV VERSION 2024.07.13
EXPOSE 8080

CMD ["/nouser/Cl4ptr4pBot"]
