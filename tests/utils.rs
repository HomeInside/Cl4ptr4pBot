use Cl4ptr4pBot::commands::{clean_str, get_local_time_zone, get_utc_offset, partial_escape, search_time_zone};

#[test]
fn test_partial_escape() {
    let message = "*examples:*\n- `/time london`\n- `/time europe/london`\n- `/time los angeles`";
    let result = partial_escape(message);
    //    println!("test_partial_escape result: {}", result);
    assert_eq!(result.is_empty(), false);

    let message_test = "*examples:*\n\\- `/time london`\n\\- `/time europe/london`\n\\- `/time los angeles`";
    assert_eq!(result, message_test);
}

#[test]
fn test_get_local_time_zone() {
    let message = "Los angeles";
    let result = get_local_time_zone(message);
    //    println!("test_get_local_time_zone result: {:?}", result);
    assert_eq!(result.is_some(), true);
    assert_eq!(result.clone().unwrap().is_empty(), false);
    assert_eq!(
        result
            .unwrap()
            .contains("Los_Angeles\nAmerica/Los_Angeles (UTC -8:00)\n"),
        true
    );
}

#[test]
fn test_clean_str() {
    let message = "Los angeles";
    let result = clean_str(message);
    //    println!("test_clean_str result: {}", result);
    assert_eq!(result.is_empty(), false);
    assert_eq!(result, "Los_Angeles");
}

#[test]
fn test_search_time_zone() {
    let message = "Los_Angeles";
    let result = search_time_zone(message);
    //    println!("test_search_time_zone result: {:?}", result);
    assert_eq!(result.is_some(), true);
    assert_eq!(result.unwrap().is_empty(), false);
    assert_eq!(result.unwrap(), "America/Los_Angeles");
}

#[test]
fn test_get_utc_offset() {
    let message = "America/Los_Angeles";
    let result = get_utc_offset(message);
    //    println!("test_get_utc_offset result: {:?}", result);
    assert_eq!(result.is_some(), true);
    assert_eq!(result.clone().unwrap().is_empty(), false);
    assert_eq!(result.unwrap(), "UTC -8:00");
}
