//! **Cl4ptr4pBot** general purpose robot manufactured by **HomeInside Inc.** It has been programmed with an overenthusiastic personality, and brags frequently, yet also expresses severe loneliness and cowardice.
//!
//! ## Getting Started
//!
//! Clone this repo and make it yours:
//!
//! ```bash,no_run
//! $ git clone git@gitlab.com:HomeInside/Cl4ptr4pBot.git
//!
//! ```
//!
//! ## Build
//!
//!```sh
//! $ cd Cl4ptr4pBot
//!```
//!
//!```sh
//! $ cargo build --release
//!```
//!
//! ## License
//!
//! **Cl4ptr4pBot** is licensed under either of the following licenses, at your option:
//!
//! - Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or [http://www.apache.org/licenses/LICENSE-2.0])
//!
//! - MIT license ([LICENSE-MIT](LICENSE-MIT) or [http://opensource.org/licenses/MIT])
//!

pub mod commands;
pub mod database;
