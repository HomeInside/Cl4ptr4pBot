use rusqlite::Error;
use rusqlite::{Connection, OpenFlags, Result};
use std::fmt;

#[derive(Debug)]
pub struct Quotes {
    quote_text: String,
}

// Similarly, implement `Display` for `Quotes`
impl fmt::Display for Quotes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "quote: {}", self.quote_text)
    }
}

const CLAPTRAP_DB: &str = "claptrap.db";

/// Realiza la conexion a SQLite.
///
/// el nombre de la base de datos está definido
/// en la constante `CLAPTRAP_DB`
///
/// # Returns
///
/// un `Result` con los resultados.
/// * `rusqlite::Connection` - el objeto de la conexión
/// * `rusqlite::Error` - en caso de error
///
/// # Examples
///
/// ```rust,no_run,ignore
/// use connect;
/// ...
///
/// connect();
/// // Ok(Connection { path: Some("/path/to/claptrap.db") })
/// ```
///
pub fn connect() -> Result<Connection> {
    let conn = Connection::open_with_flags(
        CLAPTRAP_DB,
        OpenFlags::SQLITE_OPEN_READ_WRITE | OpenFlags::SQLITE_OPEN_CREATE,
    );

    if conn.is_ok() {
        println!("connecting to: {}... is ok", CLAPTRAP_DB);
    } else {
        println!("an error occurred while connecting to the DB: {:?}", CLAPTRAP_DB);
        println!("{:?}", conn.as_ref().unwrap());
    }

    conn
}

/// Consulta una frase por su id.
///
/// # Arguments
///
/// * `conn: &Connection` - el objeto de la conexión.
/// * `id: i32` - el id del registro
///
/// # Returns
///
/// un `Result` con los resultados.
/// * `String` - la frase de la base de datos
/// * `rusqlite::Error` - en caso de error
///
/// # Examples
///
/// ```rust,no_run,ignore
/// use search_quote;
/// ...
///
/// search_quote(&db_conn, random_item);
/// // "I like this place and could willingly waste my time in it."
/// ```
///
pub fn search_quote(conn: &Connection, id: i32) -> Result<String, Error> {
    let mut stmt = conn.prepare("SELECT quote_text FROM quotes WHERE id = :id")?;

    let result = stmt.query_row([id], |row| {
        Ok(Quotes {
            quote_text: row.get(0)?,
        })
    });

    match result {
        Ok(item) => Ok(item.quote_text.to_string()),
        Err(error) => {
            println!("ocurrio un error al consultar: {}", error);
            Err(Error::QueryReturnedNoRows)
        }
    }
}
