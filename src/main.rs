use pretty_env_logger::env_logger;
use std::process;
use teloxide::prelude::*;
const TOKEN: &str = "awesome_telegram_token";
use rusqlite::Connection;
use std::sync::Arc;
use tokio::sync::Mutex;
use Cl4ptr4pBot::commands::handler_input;
use Cl4ptr4pBot::database::connect;

#[tokio::main]
async fn main() {
    println!("Welcome to {} v{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
    env_logger::builder().filter_level(log::LevelFilter::Info).init();

    println!("Starting bot...");
    let bot = Bot::new(TOKEN);

    log::info!("connecting to database...");
    let conn = connect();

    match conn {
        Ok(conn) => {
            let conn: Arc<Mutex<Connection>> = Arc::new(Mutex::new(conn));

            teloxide::repl(bot, move |bot: Bot, msg: Message| {
                let db_conn = Arc::clone(&conn);
                async move { handler_input(bot, msg, db_conn).await }
            })
            .await
        }
        Err(error) => {
            log::warn!("unable to connect to database: {}", error);
            process::exit(1);
        }
    }
}
