use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};

/// convert text to ñiñi ñiñi ñiñi! meme.
///
pub async fn nini_cmd(bot: Bot, msg: Message, message: String) -> ResponseResult<()> {
    let result = message
        .to_lowercase()
        .replace("a", "i")
        .replace("e", "i")
        .replace("o", "i")
        .replace("u", "i")
        .replace("á", "i")
        .replace("é", "i")
        .replace("í", "i")
        .replace("ó", "i")
        .replace("ú", "i");
    let text = format!("{result} ñiñi ñiñi ñi ñi!, mk!");
    bot.send_message(msg.chat.id, text).await?;
    Ok(())
}
