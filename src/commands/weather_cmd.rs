use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};

/// Consulta el Api de [wttr.in](https://wttr.in/)
/// para obtener informacion del clima.
///
pub async fn weather_cmd(bot: Bot, msg: Message, city: String) -> ResponseResult<()> {
    bot.send_chat_action(msg.chat.id, teloxide::types::ChatAction::Typing)
        .await?;

    let mut default_city = "london";

    if !city.is_empty() {
        default_city = city.as_str();
    }

    let body = reqwest::get(format!("https://wttr.in/{default_city}?format=4"))
        .await?
        .text()
        .await?;
    println!("weather result: {body:?}");

    bot.send_message(msg.chat.id, format!("Weather in: {body}")).await?;
    Ok(())
}
