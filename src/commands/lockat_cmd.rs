use crate::commands::partial_escape;
use serde_json::Value;
use teloxide::payloads::SendMessageSetters;
use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};
use teloxide::types::ParseMode::MarkdownV2;

/// show the current IP and location (if available)
///
/// Consulta el Api de [ipbase.](https://ipbase.com)
///
pub async fn lockat_cmd(bot: Bot, msg: Message) -> ResponseResult<()> {
    bot.send_chat_action(msg.chat.id, teloxide::types::ChatAction::Typing)
        .await?;

    let resp = reqwest::get("https://api.ipbase.com/v1/json/").await?.text().await?;

    // println!("lockat result: {resp:?}");
    let json_resp: Value = serde_json::from_str(resp.as_str()).unwrap();

    let message = format!(
        "Your current location:\n*IP:* {}\n*Country:* {}({})\n*City:* {}\n*Timezone:* {}\n*Zip Code:* {}\n",
        json_resp["ip"],
        json_resp["country_name"],
        json_resp["country_code"],
        json_resp["city"],
        json_resp["time_zone"],
        json_resp["zip_code"],
    );

    // println!("message: {}", message);

    let message = partial_escape(message.as_str()).replace("\"", "");

    bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;
    Ok(())
}
