use super::utils::partial_escape;
use teloxide::payloads::SendMessageSetters;
use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};
use teloxide::types::ParseMode::MarkdownV2;

/// start command
/// start this bot and show welcome message
///
pub async fn start_cmd(bot: Bot, msg: Message) -> ResponseResult<()> {
    let username = msg.from.unwrap().username.unwrap();

    let welcome_msg = format!(
        "
			Welcome: @{} :hand: 👋
			This bot is under constant development!
			If you have any question or suggestion,
			please, talk to me!

			Support the project:
			https://example.com/HomeInside/Cl4ptr4pBot.git

			version: *v{}*

			use the /help command, to look at the list of available commands",
        username,
        env!("CARGO_PKG_VERSION")
    );

    let welcome = partial_escape(&welcome_msg);

    bot.send_message(msg.chat.id, welcome).parse_mode(MarkdownV2).await?;
    Ok(())
}
