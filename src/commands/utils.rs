use chrono::{TimeZone, Utc};
use chrono_tz::{OffsetComponents, Tz, TZ_VARIANTS};
use std::cmp::Ordering;

/// Escapa algunos caracteres para que no se
/// interpreten como formato.
///
/// para más información consulta: [MarkdownV2 style](https://core.telegram.org/bots/api#markdownv2-style)
///
/// # Arguments
///
/// * `text: &str` - el texto a validar
///
/// # Returns
///
/// un [String] con los resultados.
///
/// # Examples
///
/// ```rust,no_run,ignore
/// use partial_escape;
/// ...
///
/// let message = "*examples:*\n- `/time london`\n- `/time europe/london`\n- `/time los angeles`";
/// let message = partial_escape(message);
/// // "*examples:*\n\\- `/time london`\n\\- `/time europe/london`\n\\- `/time los angeles`"
/// ```
///
pub fn partial_escape(text: &str) -> String {
    text.replace("\\", "\\\\") // Escapa las barras invertidas
        .replace("(", "\\(") // Escapa los parentesis
        .replace(")", "\\)") //
        .replace(">", "\\>") // Escapa el signo de mayor
        .replace("#", "\\#") // Escapa el símbolo de hash
        .replace("+", "\\+") // Escapa el signo más
        .replace("-", "\\-") // Escapa el guion
        .replace("_", "\\_") // Escapa el guion bajo
        .replace(".", "\\.") // Escapa el punto
        .replace("!", "\\!") // Escapa el signo de exclamación
}

/// Realiza una consulta utilizando `chrono_tz` para
/// obtener la zona horaria de una ciudad ó región.
///
/// # Arguments
///
/// * `city: &str` - el texto ingresado por
///     el usuario.
///
/// # Returns
///
/// un [Option] [String] con los resultados.
/// * `Some(String)` - el texto para enviar al bot
/// * `None` - en caso de error
///
/// # Examples
///
/// ```rust,no_run,ignore
/// use get_local_time_zone;
/// ...
///
/// get_local_time_zone("Los angeles");
/// // Los_Angeles
/// // America/Los_Angeles (UTC -8:00)
/// // Wednesday, February 26, 2025
/// // 06:33:09 PM
/// ```
///
pub fn get_local_time_zone(city: &str) -> Option<String> {
    let cleaned_city = clean_str(city);
    // println!("cleaned_city: '{}'", cleaned_city);

    let tz = search_time_zone(cleaned_city.as_str());

    match tz {
        Some(tz) => {
            println!("'{}' time zone is '{}'", city, tz);
            let tz: chrono_tz::Tz = tz.parse().unwrap();
            let utc = get_utc_offset(tz.name());
            // println!("UTC value '{}'", utc.clone().unwrap());

            let now = Utc::now().with_timezone(&tz);
            let date = now.format("%A, %B %d, %Y");
            let time = now.format("%I:%M:%S %p");

            let message = format!("{}\n{} ({})\n{}\n{}", cleaned_city, tz.name(), utc.unwrap(), date, time);

            Some(message)
        }
        None => {
            println!("'{}' time zone not found", city);
            None
        }
    }
}

/// Limpia la entrada del usuario.

/// - convierte cada palabra en letra capital (titlecase)
/// - convierte los espacios en blanco a guiones bajos (_)
///
/// # Arguments
///
/// * `dirty_string: &str` - el texto ingresado por
///     el usuario.
///
/// # Returns
///
/// un [String] con la conversión realizada
///
/// # Examples
///
/// ```rust,no_run,ignore
/// use clean_str;
/// ...
///
/// clean_str("Los angeles");
/// // Los_Angeles
/// ```
///
pub fn clean_str(dirty_string: &str) -> String {
    dirty_string
        .split_whitespace()
        .map(|word| word.chars().next().unwrap().to_uppercase().to_string() + &word[1..].to_lowercase())
        .collect::<Vec<_>>()
        .join("_")
}

/// Busca la zona horaria de una ciudad ó región, a partir
/// de un texto ingresado.
///
/// # Arguments
///
/// * `city: &str` - el texto a buscar
///
/// # Returns
///
/// un [Option] `&'static str`
/// * `Some(&'static str)` - Si se encuentra un
///     resultado que coincida con la busqueda
/// * `None` - en caso de error
///
/// # Examples
///
/// ```rust,no_run,ignore
/// use search_time_zone;
/// ...
///
/// search_time_zone("los angeles");
/// // America/Los_Angeles
/// ```
///
pub fn search_time_zone(city: &str) -> Option<&'static str> {
    for item in TZ_VARIANTS.iter() {
        if item.name().to_lowercase().contains(&city.to_lowercase()) {
            return Some(item.name());
        }
    }
    None
}

/// Convierte la zona horaria a su correspondiente
/// valor en UTC.
///
/// # Arguments
///
/// * `tz_name: &str` - la zona horaria
///
/// # Returns
///
/// un [Option] [String]
/// * `Some(String)` - Si el valor se genera
///     correctamente
/// * `None` - en caso de error
///
/// # Examples
///
/// ```rust,no_run,ignore
/// use get_utc_offset;
/// ...
///
/// get_utc_offset("America/Los_Angeles");
/// // (UTC -8:00)
/// ```
///
pub fn get_utc_offset(tz_name: &str) -> Option<String> {
    let tz: Tz = tz_name.parse().unwrap();
    let utc = Utc::now().naive_utc();
    let offset = tz.offset_from_utc_datetime(&utc);

    let hours = offset.base_utc_offset().num_hours() + offset.dst_offset().num_hours();
    let minutes = (offset.base_utc_offset().num_minutes() + offset.dst_offset().num_minutes()) % 60;
    // println!("get_utc_offset hours: '{}'", hours);
    // println!("get_utc_offset minutes: '{}'", minutes);

    let sign = match hours.cmp(&0) {
        Ordering::Less => "-",
        Ordering::Greater => "+",
        Ordering::Equal => "",
    };

    Some(format!("UTC {}{}:{:02}", sign, hours.abs(), minutes.abs()))
}
