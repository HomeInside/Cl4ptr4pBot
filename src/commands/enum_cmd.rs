use teloxide::utils::command::BotCommands;

/// Permite definir los comandos del bot.
#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "These commands are supported:")]
pub enum BotCommand {
    #[command(description = "this command.")]
    Help,
    #[command(description = "start this bot")]
    Start,
    #[command(description = "retrieve ping")]
    Ping,
    #[command(description = "show the current time")]
    Time(String),
    #[command(description = "show the current IP")]
    Ip,
    #[command(description = "show the current IP and location (if available)")]
    LockAt,
    #[command(description = "show the current weather info")]
    Weather(String),
    #[command(description = "Starting throw dice bot")]
    Dice,
    #[command(description = "current version of Cl4ptr4pBot, with emoji support")]
    Emoji,
    #[command(description = "convert text to ñiñi ñiñi ñiñi! meme.")]
    Nini(String),
    #[command(description = "sample command to test info.")]
    Sample,
    #[command(description = "about Cl4ptr4pBot")]
    About,
    #[command(description = "current version of Cl4ptr4pBot")]
    Version,
}
