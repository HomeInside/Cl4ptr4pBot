use super::*;
use crate::database::search_quote;
use rand::rngs::StdRng;
use rand::Rng;
use rand::SeedableRng;
use rusqlite::Connection;
use std::sync::Arc;
use teloxide::types::ParseMode::MarkdownV2;
use teloxide::utils::markdown;
use teloxide::{prelude::*, utils::command::BotCommands};
use tokio::sync::Mutex;

/// gestiona la entrada desde el usuario y valida si es un
/// comando ó un texto general.
///
pub async fn handler_input(bot: Bot, msg: Message, conn: Arc<Mutex<Connection>>) -> ResponseResult<()> {
    // log::info!("handler_input fn...");
    if let Some(user_input) = msg.text() {
        if !user_input.is_empty() {
            if let Ok(cmd) = BotCommand::parse(user_input, env!("CARGO_PKG_NAME")) {
                handler_cmd(bot, msg, cmd).await?;
            } else {
                handler_messages(bot, msg, conn).await?;
            }
        }
    }

    Ok(())
}

/// maneja los comandos del bot.
///
pub async fn handler_cmd(bot: Bot, msg: Message, cmd: BotCommand) -> ResponseResult<()> {
    // log::info!("handler_cmd fn...");
    match cmd {
        BotCommand::Help => {
            let cmd_msg = BotCommand::descriptions().to_string();
            bot.send_message(msg.chat.id, cmd_msg).await?;
        }
        BotCommand::Start => {
            start_cmd(bot, msg).await?;
        }
        BotCommand::Ping => {
            bot.send_message(msg.chat.id, "pong").await?;
        }

        BotCommand::Time(city) => {
            time_cmd(bot, msg, city).await?;
        }
        BotCommand::Ip => {
            ip_cmd(bot, msg).await?;
        }
        BotCommand::LockAt => {
            lockat_cmd(bot, msg).await?;
        }
        BotCommand::Weather(city) => {
            weather_cmd(bot, msg, city).await?;
        }
        BotCommand::Dice => {
            bot.send_dice(msg.chat.id).await?;
        }
        BotCommand::Emoji => {
            bot.send_message(msg.chat.id, "Emoji: not implemented yet!").await?;
        }
        BotCommand::Nini(message) => {
            nini_cmd(bot, msg, message).await?;
        }
        BotCommand::Sample => {
            sample_cmd(bot, msg).await?;
        }
        BotCommand::About => {
            about_cmd(bot, msg).await?;
        }
        BotCommand::Version => {
            let message = format!("Cl4ptr4pBot *v{}*", env!("CARGO_PKG_VERSION"));
            let message = partial_escape(message.as_str());

            bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;
        }
    };
    Ok(())
}

/// maneja el texto entrante (texto general).
///
pub async fn handler_messages(bot: Bot, msg: Message, conn: Arc<Mutex<Connection>>) -> ResponseResult<()> {
    log::info!("handler_messages fn...");
    log::info!("chat msg: {:?}", msg.text().unwrap());
    bot.send_chat_action(msg.chat.id, teloxide::types::ChatAction::Typing)
        .await?;

    bot.send_message(
        msg.chat.id,
        "this Bot will recite Shakespeare quotes as you type any text...",
    )
    .await?;

    bot.send_chat_action(msg.chat.id, teloxide::types::ChatAction::Typing)
        .await?;

    // Crear un nuevo generador de números aleatorios
    // cumpliendo con `Send + Sync + 'static`
    let mut rng = StdRng::from_entropy();
    let random_item = rng.gen_range(1..=50);
    //    let random_item = 12;
    log::info!("random_item: {}", random_item);

    let db_conn = conn.lock().await;
    let result = search_quote(&db_conn, random_item);
    drop(db_conn);

    match result {
        Ok(quote) => {
            let message = markdown::escape(quote.as_str());
            let message = markdown::blockquote(message.replace("\n", "\n>").as_str());
            bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;
        }
        Err(error) => {
            log::warn!("no data from database, error: {}", error);
            let message = "Hark! Is this a conundrum that I spy? Forsooth, 'tis but a pebble in the sandal of my life. Methinks even the stars doth conspire to render mine brain a treacherous labyrinth!";
            let message = markdown::escape(message);
            bot.send_message(msg.chat.id, markdown::italic(message.as_str()))
                .parse_mode(MarkdownV2)
                .await?;

            let error_msg = markdown::blockquote(error.to_string().as_str());
            bot.send_message(msg.chat.id, error_msg).parse_mode(MarkdownV2).await?;
        }
    }

    bot.send_message(
        msg.chat.id,
        "or use the /help command, to look at the list of available commands",
    )
    .await?;

    Ok(())
}
