use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};

/// show the current IP
pub async fn ip_cmd(bot: Bot, msg: Message) -> ResponseResult<()> {
    bot.send_chat_action(msg.chat.id, teloxide::types::ChatAction::Typing)
        .await?;

    let body = reqwest::get("https://icanhazip.com").await?.text().await?;
    println!("ip: {body:?}");

    bot.send_message(msg.chat.id, format!("IP: {body}")).await?;
    Ok(())
}
