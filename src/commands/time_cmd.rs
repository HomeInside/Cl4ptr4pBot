use super::utils::get_local_time_zone;
use super::utils::partial_escape;
use teloxide::payloads::SendMessageSetters;
use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};
use teloxide::types::ParseMode::MarkdownV2;

/// Muestra la fecha, la hora actual y la zona horaria
/// de una ciudad ó región.
///
pub async fn time_cmd(bot: Bot, msg: Message, city: String) -> ResponseResult<()> {
    bot.send_chat_action(msg.chat.id, teloxide::types::ChatAction::Typing)
        .await?;

    let mut default_city = "london";

    if !city.is_empty() {
        default_city = city.as_str();
    }

    let tz = get_local_time_zone(default_city);

    match tz {
        Some(message) => {
            // println!("message: {}", message);
            let message = partial_escape(message.as_str()).replace("\"", "");
            bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;
        }
        None => {
            println!("'{}' time zone not found", city);
            let message = format!(
                "an error occurred, when consulting the date and time of '*{}*', check the name of the city, and try again.",
                city
            );
            let message = partial_escape(message.as_str());
            bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;

            let message = "*examples:*\n- `/time london`\n- `/time europe/london`\n- `/time los angeles`";
            let message = partial_escape(message);
            bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;
        }
    }
    Ok(())
}
