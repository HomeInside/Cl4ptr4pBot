use super::utils::partial_escape;
use teloxide::payloads::SendMessageSetters;
use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};
use teloxide::types::ParseMode::MarkdownV2;

/// sample command to test info.
///
pub async fn sample_cmd(bot: Bot, msg: Message) -> ResponseResult<()> {
    let message =
        "'Our *doubts* are _traitors_, And __make us__ ~lose~ the ||good|| we oft `might win`, By fearing to [attempt.](https://www.panmacmillan.com/blogs/classics/our-favourite-shakespeare-quotes)'";
    let message = partial_escape(message);

    bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;

    Ok(())
}
