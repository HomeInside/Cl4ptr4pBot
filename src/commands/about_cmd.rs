use super::utils::partial_escape;
use teloxide::payloads::SendMessageSetters;
use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message, ResponseResult};
use teloxide::types::ParseMode::MarkdownV2;
use teloxide::utils::markdown;

/// about command
///
pub async fn about_cmd(bot: Bot, msg: Message) -> ResponseResult<()> {
    let link = markdown::link("https://gitlab.com/HomeInside/", "HomeInside Inc.");

    let message = format!(
        "*Cl4ptr4pBot* general purpose robot manufactured by {}\n\
            It has been programmed with an overenthusiastic personality, \
            and brags frequently, yet also expresses severe loneliness and cowardice.",
        link
    );

    let message = partial_escape(message.as_str());

    bot.send_message(msg.chat.id, message).parse_mode(MarkdownV2).await?;
    Ok(())
}
