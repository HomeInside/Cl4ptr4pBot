
# Cl4ptr4p Bot :robot:

## Description
**Cl4ptr4pBot** general purpose robot manufactured by **HomeInside Inc.** It has been programmed with an overenthusiastic personality, and brags frequently, yet also expresses severe loneliness and cowardice.


## About
manufactured by [HomeInside Inc.](https://gitlab.com/HomeInside)

This bot is under constant development!
If you have any question or suggestion please, talk to me!


## see in action:
[Cl4ptr4pBot in telegram](https://telegram.me/Cl4ptr4pBot)


## Botpic
:no_entry_sign: no botpic.


## Commands
It is currently programmed with powerful commands:

- `/help` - [this command]
- `/start` - [start bot]
- `/ping` - [retrieve ping]
- `/time` - [show the current time]
- `/ip` - [show the current IP]
- `/lockat` - [show the current IP and location (if available)]
- `/weather` - [show the current weather info]
- `/dice` - [Starting throw dice bot]
- `/emoji` - [current version of Cl4ptr4pBot, with emoji support]
- `/nini` - [convert text to ñiñi ñiñi ñiñi! meme]
- `/sample` - [sample command to test info.]
- `/about` - [about Cl4ptr4pBot]
- `/version` - [current version of Cl4ptr4pBot]


## Documentation
this file.

- [telegram bots api](https://core.telegram.org/bots/api)
- [teloxide](https://crates.io/crates/teloxide)
- [rusqlite](https://crates.io/crates/rusqlite)


## Requirements

 - [rustup](https://rustup.rs) to install Rust :wink:
 - [Rust](https://www.rust-lang.org) **1.81.0** and above
 - [just](https://github.com/casey/just) **1.35** and above (to manage tasks)


## Getting Started

Clone this repo and make it yours:

```sh
$ git clone git@gitlab.com:HomeInside/Cl4ptr4pBot.git
```

### Build

```sh
$ cd Cl4ptr4pBot
```

```sh
$ cargo build --release
```


## Run the bot
Until the you presses **Ctrl-C** or the process receives **SIGINT**, **SIGTERM** or **SIGABRT**.

This should be used most of the time, since `start_polling()` is non-blocking and will stop the bot gracefully.

```sh
$ cargo run
# or
$ ./target/release/Cl4ptr4pBot
```


## Thanks to
- [Frankity](https://github.com/Frankity)
- [jac08h/DrinkMixerBot](https://github.com/jac08h/DrinkMixerBot)
- [SumitAgr/PictureofTheDay-Bot](https://github.com/SumitAgr/PictureofTheDay-Bot)
- [Nhoya/YATAB](https://github.com/Nhoya/YATAB)
- [wttr.in](https://wttr.in)
- [icanhazip](https://icanhazip.com/)
- [risoflora/wethr](https://github.com/risoflora/wethr)
- [ipbase](https://ipbase.com)
- [opensourceshakespeare](https://www.opensourceshakespeare.org)
- [goodreads/William_Shakespeare](https://www.goodreads.com/author/quotes/947.William_Shakespeare)
- [panmacmillan](https://www.panmacmillan.com/blogs/classics/our-favourite-shakespeare-quotes)
- [shakespeare.org.uk](https://www.shakespeare.org.uk/)


## License

**Cl4ptr4pBot** is licensed under either of the following licenses, at your option:

- Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or [http://www.apache.org/licenses/LICENSE-2.0])
- MIT license ([LICENSE-MIT](LICENSE-MIT) or [http://opensource.org/licenses/MIT])

