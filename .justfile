# Just task runner

# 2025-02-19
set dotenv-load := false
just_home := justfile_directory()

# help
default: help

# show justfile config file
help:
    @echo
    @echo "The config file is at: {{ just_home }}"
    @echo "projects tasks:"
    @echo
    @echo "- fmt"
    @echo "- check"
    @echo "- fix"
    @echo "- clippy"
    @echo "- run"
    @echo "- build"
    @echo "- release"
    @echo "- docs"
    @echo "- dist"
    @echo "- test"
    @echo "- clean"
    @echo

fmt:
	clear && cargo fmt

check: fmt
	cargo check --locked

fix: fmt check

clippy: fmt
	cargo clippy --no-deps --locked

run: fmt
	cargo run

build: fmt
	cargo build --locked

release: fmt
	build --release --locked

docs: fmt
	RUSTDOCFLAGS="--cfg=docsrs" cargo doc --no-deps --document-private-items --release

dist-r:
    clear && ./target/release/Cl4ptr4pBot

dist-d:
    clear && ./target/debug/Cl4ptr4pBot

test: fmt
    # cargo test
    cargo test --locked

# cleaning the house...
clean:
    clear
    @echo "cleaning the house..."
    # cargo clean
